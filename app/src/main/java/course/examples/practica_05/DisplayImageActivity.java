package course.examples.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class DisplayImageActivity extends AppCompatActivity {

    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_image);

        Intent intent  = getIntent();
        String sel = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        int val = Integer.parseInt(sel);

        image = (ImageView) findViewById(R.id.imageView1);

        if(val == 1){
            image.setImageResource(R.drawable.uva);
        }else if(val == 2){
            image.setImageResource(R.drawable.pina);
        }else if(val == 3){
            image.setImageResource(R.drawable.manzana);
        }else if(val == 4){
            image.setImageResource(R.drawable.sandia);
        }
    }
}
